-- Active: 1709545618310@@127.0.0.1@3306@Usecase_exo

DROP TABLE  IF EXISTS student_promo;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS student; 
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region;


CREATE TABLE region (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE center (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    id_region INT,
    Foreign Key (id_region) REFERENCES region(id) ON DELETE CASCADE
);

CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    capacity INT NOT NULL,
    color VARCHAR (50) NOT NULL,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE CASCADE
);

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    startDate DATETIME,
    endDate DATETIME,
    studentNumber INT,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE CASCADE
);

CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    id_promo INT,
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE SET NULL
);

CREATE TABLE booking (
    id INT PRIMARY KEY AUTO_INCREMENT,
    startDate DATETIME,
    endDate DATETIME,
    id_room INT,
    Foreign Key (id_room) REFERENCES room(id) ON DELETE SET NULL,
    id_promo INT,
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE SET NULL
);

CREATE TABLE student_promo(
    id_promo INT,
    id_student INT,
    PRIMARY KEY (id_promo,id_student),
    Foreign Key (id_promo) REFERENCES promo(id),
    Foreign Key (id_student) REFERENCES student(id) 
);


INSERT INTO region (name) VALUES
    ('Rhone');

INSERT INTO center (name, id_region) VALUES
    ('Lyon', 1),
    ('Grenoble',1),
    ('Annecy',1);

INSERT INTO room (name, capacity, color,id_center) VALUES
    ('Cuisine', 25, 'blue',1),
    ('Chambre', 30, 'purple',2),
    ('Douche', 20, 'pink',3),
    ('Amphi', 70, 'yellow',1),
    ('Jeux', 15, 'orange',2),
    ('Salon', 35, 'red',3);

INSERT INTO promo (name, startDate, endDate, studentNumber, id_center) VALUES
    ('CP', '2023-02-12', '2024-01-01', 22, 1),
    ('CE1', '2022-12-05', '2023-02-28', 25, 2),
    ('CE2', '2024-08-23', '2025-05-04', 18, 3),
    ('CM1', '2022-06-08', '2023-04-18', 55, 1),
    ('CM2', '2024-01-26', '2025-02-26', 20, 2);

INSERT INTO student (name) VALUES
    ('Sarah'),
    ('Matthieu'),
    ('Ismael'),
    ('Chieko'),
    ('Aymeric'),
    ('Oceane'),
    ('El-Amine'),
    ('Amine'),
    ('Djino'),
    ('Jules');



INSERT INTO booking (id_room, id_promo, startDate, endDate) VALUES
    (1,2, '2024-03-04', '2024-03-08'),
    (2,1, '2024-02-15', '2024-04-12'),
    (3,4, '2024-01-12', '2024-03-05'),
    (4,5, '2024-06-04', '2024-08-09'),
    (5,3, '2024-04-09', '2024-05-07');

INSERT INTO student_promo (id_student, id_promo) VALUES
    (1,1),
    (1,2),
    (2,3),
    (3,4),
    (4,5),
    (5,3),
    (5,1),
    (6,2),
    (7,3),
    (8,4),
    (9,5),
    (9,1),
    (10,1);
