-- Active: 1709545618310@@127.0.0.1@3306@Usecase_exo

--Lister les salles d'un centre
SELECT room.* FROM room
JOIN center ON room.id_center=center.id
WHERE center.name="Lyon";

--Lister les promo d'un centre 
SELECT promo.* FROM promo
JOIN center ON promo.id_center=center.id
WHERE center.name="Lyon";

--Modifier la liste des apprenant
UPDATE student
SET name = 'Lola'
WHERE id = 2;

--Afficher le planning du jour 
SELECT room.name, promo.name FROM booking
LEFT JOIN room ON booking.id_room = room.id
LEFT JOIN promo ON booking.id_promo = promo.id
WHERE DATE (booking.startDate)= CURDATE();

--Afficher le planning complet 
--Par salle  
SELECT room.name, promo.name, booking.startDate, booking.endDate FROM booking
LEFT JOIN room ON booking.id_room = room.id
LEFT JOIN promo ON booking.id_promo = promo.id
WHERE room.name='Cuisine';

--Par promo 
SELECT promo.name, room.name, booking.startDate, booking.endDate FROM booking
LEFT JOIN room ON booking.id_room = room.id
LEFT JOIN promo ON booking.id_promo = promo.id
WHERE promo.name='CM2';

--Changer la période
UPDATE booking
SET startDate = '2023-12-22', endDate='2024-06-12'
WHERE id = 1;